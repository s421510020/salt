keepalived-install:
  file.managed:
    - name: /usr/local/src/keepalived-1.2.8.tar.gz
    - source: salt://keepalived/files/keepalived-1.2.8.tar.gz
    - mode: 755
    - user: root
    - group: root
  cmd.run:
    - name: cd /usr/local/src && tar zxf keepalived-1.2.8.tar.gz && cd keepalived-1.2.8 && ./configure --prefix=/jboss/keepalived-1.2.8 && make && make install
    - unless: test -d /jboss/keepalived-1.2.8
    - require:
      - file: keepalived-install

/etc/sysconfig/keepalived:
  file.managed:
    - source: salt://keepalived/files/keepalived.sysconfig
    - mode: 644
    - user: root
    - group: root

/etc/init.d/keepalived:
  file.managed:
    - source: salt://keepalived/files/keepalived.init
    - mode: 755
    - user: root
    - group: root

keepalived-server:
  file.managed:
    - name: /etc/keepalived/keepalived.conf
    - source: salt://keepalived/files/keepalived.conf
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    {% if grains['fqdn'] == 'file-node1.womai.com' %}
    - ROUTEID: HAPROXY_MASTER
    - STATEID: MASTER
    - PRIORITYID: 101
    {% elif grains['fqdn'] == 'file-node2.womai.com' %}
    - ROUTEID: HAPROXY_BACKUP
    - STATEID: BACKUP
    - PRIORITYID: 100
    {% endif %}
  cmd.run:
    - name: chkconfig --add keepalived
    - unless: chkconfig --list | grep keepalived
    - require:
      - file: /etc/init.d/keepalived
  service.running:
    - name: keepalived
    - enable: True
    - watch: 
      - file: keepalived-server
