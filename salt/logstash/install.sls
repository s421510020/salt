jdk-install:
  file.managed:
    - name: /opt/logstash/jdk-7u51-linux-x64.tar.gz
    - source: salt://logstash/files/jdk-7u51-linux-x64.tar.gz
    - user: root
    - group: root
    - mode: 644
  cmd.run:
    - name: cd /opt/logstash/ && tar zxf jdk-7u51-linux-x64.tar.gz && chown -R root:root jdk1.7.0_51
    - unless: test -d /opt/logstash/jdk1.7.0_51

logstash-install:
  file.managed:
    - name: /opt/logstash/logstash-1.3.3-flatjar.jar
    - source: salt://logstash/files/logstash-1.3.3-flatjar.jar
    - user: root
    - group: root
    - mode: 644

logstash-init:
  file.managed:
    - name: /etc/init.d/logstash
    - source: salt://logstash/files/logstash.sh
    - user: root
    - group: root
    - mode: 755

  cmd.run:
    - name: chkconfig --add logstash && chkconfig logstash on
    - unless: chkconfig --list | grep logstash
    - require:
      - file: logstash-init
