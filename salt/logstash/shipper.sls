include: 
  - logstash.install

logstash-config:
  file.managed:
    - name: /opt/logstash/logstash.conf
    - source: salt://logstash/files/shipper.conf
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - defaults:
      IPADDR: {{ salt['network.ip_addrs']()[0] }}

logstash-service:
  service.running:
    - name: logstash
    - enable: True
    - reload: True
    - watch:
      - file: logstash-config
    - require:
      - cmd.run: logstash-init
