addrepo:
  file.managed:
    - name: /etc/yum.repos.d/logstash.repo
    - source: salt://repo/files/logstash.repo
    - unless: test -f /etc/yum.repos.d/logstash.repo
  cmd.run:
    - name: rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch
	
logstash-install:
  pkg.installed:
    - pkg: logstash
  file.managed:
    - name: /etc/logstash/conf.d/logstash.conf
    - source: salt://logstash/files/logstash.conf
  service.running:
    - name: logstash
    - enable: True
    - reload: True
    - watch:
      - file: /etc/logstash/conf.d/logstash.conf
