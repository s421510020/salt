adduser:
  user.present:
    - name: {{ pillar['user'] }}
    - gid_from_name: True
    - createhome: True
    - password: {{ pillar['passwd'] }}
    - unless: id {{ pillar['user'] }} 
