{% set temp = pillar['temp'] %}
{% set sites = pillar['sites'] %}
{% set appserver = pillar['appserver'] %}
mkdir:
  cmd.run:
    - names:
      - mkdir -p {{ temp }} && mkdir -p {{ sites }} && mkdir -p {{ appserver }}
    - unless: test -d {{ appserver }}
