elasticsearch-install:
  file.managed:
    - name: /opt/logstash/elasticsearch-1.0.0.tar.gz
    - source: salt://logstash/files/elasticsearch-1.0.0.tar.gz
    - user: root
    - group: root
    - mode: 644
  cmd.run:
    - name: cd /opt/logstash && tar zxf elasticsearch-1.0.0.tar.gz
    - unless: test -d /opt/logstash/elasticsearch-1.0.0

  
