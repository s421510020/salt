include:
  - set.init

{% set temp = pillar['temp'] %}
{% set appserver = pillar['appserver'] %}
{% set memcached = pillar['memcached'] %}
{% set memcached_ver = pillar['memcached_ver'] %}

memcached_source:
  file.managed:
    - name: {{temp}}/{{memcached}}
    - unless: test -e {{temp}}/{{memcached}}
    - source: salt://memcached/files/{{memcached}}

extract_memcached:
  cmd.run:
    - cwd: {{temp}}
    - name: tar zxf {{memcached}}
    - unless: test -d {{temp}}/{{memcached_ver}}
    - require:
      - file: memcached_source

memcached-pkgs:
  pkg.installed:
    - pkgs:
      - libevent-devel
memcached_compile:
  cmd.run:
    - cwd:  {{temp}}/{{memcached_ver}}
    - name: ./configure --prefix={{appserver}}/{{memcached_ver}} --with-libevent && make && make install
    - require:
      - cmd: extract_memcached
      - pkg: memcached-pkgs
    - unless: test -d {{appserver}}/{{memcached_ver}}

