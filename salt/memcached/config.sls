include:
  - memcached.install

{% set memcached_ver = pillar['memcached_ver'] %}
{% set appserver = pillar['appserver'] %}

##service for memcached

tran_script:
  file.managed:
    - name: /etc/init.d/memcached
    - source: salt://memcached/files/memcached-init
    - user: root
    - mode: 755
    - template: jinja
    - require:
       - cmd: memcached_compile

  cmd.run:
    - names:
      - chkconfig --add memcached
      - chkconfig memcached on
    - unless: chkconfig --list memcached


start_script:
  service.running:
    - name: memcached
    - enable: True
    - reload: True
    - watch:
      - file: /etc/init.d/memcached

