addrepo:
  file.managed:
    - name: /etc/yum.repos.d/logstash.repo
    - source: salt://repo/files/logstash.repo
    - unless: test -f /etc/yum.repos.d/logstash.repo
  cmd.run:
    - name: rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch
  pkg.installed:
    - pkg: logstash
