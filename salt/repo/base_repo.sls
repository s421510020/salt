
{% set temp = pillar['temp'] %}
add-rpm-repo:
  file.managed:
    - name: {{temp}}/rpmforge-release-0.5.2-2.el6.rf.x86_64.rpm
    - source: salt://repo/files/rpmforge-release-0.5.2-2.el6.rf.x86_64.rpm
    - unless: test -f {{temp}}/rpmforge-release-0.5.2-2.el6.rf.x86_64.rpm
add-epel-repo:
  file.managed:
    - name: {{temp}}/epel-release-6-8.noarch.rpm
    - source: salt://repo/files/epel-release-6-8.noarch.rpm
    - unless: test -f {{temp}}/epel-release-6-8.noarch.rpm
install-repo:
  cmd.run:
    - cwd: {{temp}}
    - name: rpm -ivh epel-release-6-8.noarch.rpm && rpm -ivh rpmforge-release-0.5.2-2.el6.rf.x86_64.rpm

