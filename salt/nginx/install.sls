  
{% set nginx = pillar['nginx'] %}
{% set nginx_ver = pillar['nginx_ver'] %}
{% set appserver = pillar['appserver'] %}
{% set temp = pillar['temp'] %}
{% set pcre = pillar['pcre'] %}
{% set pcre_ver = pillar['pcre_ver'] %}

nginx_source:
  file.managed:
    - name: {{ temp }}/{{ nginx }}
    - unless: test -e {{ temp }}/{{ nginx }}
    - source: salt://nginx/files/{{ nginx }}

#pcre.tar.gz
pcre_source:
  file.managed:
    - name: {{ temp }}/{{ pcre }}
    - unless: test -e {{ temp }}/{{ pcre }}
    - source: salt://nginx/files/{{ pcre }}

#extract
extract_nginx:
  cmd.run:
    - cwd: {{ temp }}
    - names:
      - tar zxf {{ nginx }}
    - unless: test -d {{ temp }}/{{ nginx_ver }}
    - require:
      - file: nginx_source

extract_pcre:
  cmd.run:
    - cwd: {{ temp }}
    - names:
      - tar zxf {{ pcre }}
    - unless: test -d {{ temp }}/{{ pcre_ver }}
    - require:
      - file: pcre_source

#user
nginx_user:
  user.present:
    - name: nginx
    - uid: 1501
    - createhome: False
    - gid_from_name: True
    - shell: /sbin/nologin

#nginx_pkgs
nginx_pkg:
  pkg.installed:
    - pkgs:
      - gcc
      - openssl-devel
      - gcc-c++
      - zlib-devel
      - pcre-devel


#pcre_compile
pcre_compile:
  cmd.run:
    - cwd: {{ temp }}/{{ pcre_ver }}
    - names:
      - ./configure --prefix={{ appserver }}/pcre && make && make install
    - require:
      - cmd: extract_pcre
    - unless: test -d {{ appserver }}/pcre

#nginx_compile
nginx_compile:
  cmd.run:
    - cwd: {{ temp }}/{{ nginx_ver }}
    - names:
      - ./configure --prefix={{ appserver }}/{{ nginx_ver }}  --user=nginx  --group=nginx   --with-http_stub_status_module --with-http_ssl_module --with-http_gzip_static_module --with-http_realip_module --with-ipv6  --with-pcre  --with-poll_module  && make && make install
    - require:
      - cmd: extract_nginx
      - pkg: nginx_pkg
    - unless: test -d {{ appserver }}/{{ nginx_ver }}

#cache_dir
cache_dir:
  cmd.run:
    - names:
      - chown -R nginx.nginx {{ appserver }}/{{ nginx_ver }} && mkdir -p {{ appserver }}/{{ nginx_ver }}/conf/vhosts
    - require:
      - cmd: nginx_compile
