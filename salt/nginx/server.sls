{% set nginx_ver = pillar['nginx_ver'] %}
{% set appserver = pillar['appserver'] %}


{{ appserver }}/{{ nginx_ver}}/conf/nginx.conf:
  file.managed:
    - source: salt://nginx/files/nginx.conf
    - mode: 644
    - user: nginx
    - group: nginx
    - template: jinja
nginx-server:
  file.managed:
    - name: /etc/init.d/nginx
    - source: salt://nginx/files/nginx-init
    - mode: 755
    - user: root
    - group: root
    - template: jinja
  cmd.run:
    - name: chkconfig --add nginx && chkconfig nginx on
    - unless: chkconfig --list | grep nginx
    - require:
      - file: nginx-server
  service.running:
    - name: nginx
    - enable: True
    - reload: True
    - watch:
      - file: {{ appserver }}/{{ nginx_ver}}/conf/nginx.conf
    - require:
      - cmd: nginx-server
