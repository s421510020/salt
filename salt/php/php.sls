php-install:
  pkg.installed:
    - names:
      - gcc
      - glibc
      - autoconf
      - libjpeg-turbo
      - libjpeg-turbo-devel
      - libpng
      - libpng-devel 
      - freetype
      - freetype-devel
      - libxml2
      - libxml2-devel
      - zlib
      - zlib-devel
      - libcurl
      - libcurl-devel
  file.managed:
    - name: /usr/local/src/php-5.5.13.tar.gz
    - source: salt://php/files/php-5.5.13.tar.gz
    - user: root
    - group: root
    - mode: 755
  cmd.run:
    - name: cd /usr/local/src && tar zxf php-5.5.13.tar.gz && cd php-5.5.13 && ./configure --prefix=/usr/local/php --with-apxs2=/usr/local/httpd/bin/apxs  --with-mysql --with-jpeg-dir --with-png-dir --with-zlib --enable-xml --with-libxml-dir --with-curl --enable-bcmath --enable-shmop --enable-sysvsem  --enable-inline-optimization --enable-mbregex --with-openssl --enable-mbstring --with-gd --enable-gd-native-ttf --enable-sockets --with-xmlrpc --enable-zip --enable-soap --disable-debug --enable-opcache --enable-zip && make && make install
    - unless: test -d /usr/local/php
  require:
    - file: php-install
    - pkg.installed: php-install
