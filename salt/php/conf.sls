

{% set php = pillar['php'] %}
{% set php_ver = pillar['php_ver'] %}
{% set appserver = pillar['appserver'] %}
{% set temp = pillar['temp'] %}
{% for id in range(5) %}
{{ appserver }}/{{ php_ver }}/etc/php-fpm{{ id }}.conf:
  file.managed:
    - source: salt://php/files/php-fpm{{ id }}.conf
    - user: root
    - group: root
    - mode: 644
    - unless: test -e {{ appserver }}/{{ php_ver }}/etc/php-fpm{{ id }}.conf
    - template: jinja
{% endfor %}

copy-php.ini:
  cmd.run:
    - cwd: {{ appserver }}/{{ php_ver }}/lib
    - name: cp {{ temp }}/{{ php_ver }}/php.ini-production php.ini
    - user: root
    - group: root
    - mode: 644
    - unless: test -f php.ini

mkdir_log:
  cmd.run:
    - cwd: {{ appserver }}/{{ php_ver }}
    - name: mkdir log
    - user: root
    - group: root
    - mode: 644
    - unless: test -d {{ appserver }}/{{ php_ver }}/log

start_init:
  file.managed:
    - name: /etc/init.d/php-fpm
    - source: salt://php/files/php-init
    - user: root
    - group: root
    - mode: 755
    - template: jinja
  service.running:
    - name: php-fpm
    - enable: True
    - reload: True
    - watch:
      - file: /etc/init.d/php-fpm
    - require:
      - file: start_init