
{% set php = pillar['php'] %}
{% set php_ver = pillar['php_ver'] %}
{% set appserver = pillar['appserver'] %}
{% set temp = pillar['temp'] %}
{% set mysql_ver = pillar['mysql_ver'] %}


php-pkg:
  pkg.installed:
    - names:
      - gcc
      - glibc
      - autoconf
      - libjpeg-turbo
      - libjpeg-turbo-devel
      - libpng
      - libpng-devel 
      - freetype
      - freetype-devel
      - libxml2
      - libxml2-devel
      - zlib
      - zlib-devel
      - libcurl
      - libcurl-devel
      - libmcrypt-devel
  file.managed:
    - name: {{ temp }}/{{ php }}
    - source: salt://php/files/{{ php }}
    - user: root
    - group: root
    - mode: 755
php-exact:
  cmd.run:
    - cwd: {{ temp }}
    - name: tar zxf {{ php }}
  require:
    - pkg.install: php-pkg
php-install:
  cmd.run:
    - cwd: {{ temp }}/{{ php_ver }}
    - name: ./configure --prefix={{ appserver }}/{{ php_ver }} --with-mysql={{ appserver }}/{{ mysql_ver }} --enable-fpm --with-jpeg-dir --with-png-dir --with-zlib --enable-xml --with-libxml-dir --with-curl --enable-bcmath --enable-shmop --enable-sysvsem  --enable-inline-optimization --enable-mbregex --with-openssl --enable-mbstring --with-gd --enable-gd-native-ttf --enable-sockets --with-xmlrpc --enable-zip --enable-soap --disable-debug --enable-opcache --enable-ftp --with-mysqli --with-pdo-mysql --enable-mysqlnd --with-mcrypt --with-freetype-dir && make && make install
    - unless: test -d {{ appserver }}/{{ php_ver }}
  require:
    - cmd: php-exact
    - pkg.installed: php-install
