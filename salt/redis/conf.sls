include:
  - redis.install

{% set redis_ver = pillar['redis_ver'] %}
{% set appserver = pillar['appserver'] %}

mk_dir:
  cmd.run:
    - cwd: {{appserver}}/{{redis_ver}}
    - name: mkdir {data,log,conf}
    - unless: test -d data | test -d log | test -d conf
    - require:
      - cmd: redis_compile

redis_conf:
  file.managed:
    - name: {{appserver}}/{{redis_ver}}/conf/redis.conf
    - user: root
    - group: root
    - mode: 644
    - source: salt://redis/files/redis_sample.conf
    - template: jinja


##service for redis

tran_script:
  file.managed:
    - name: /etc/init.d/redis
    - source: salt://redis/files/{{redis_ver}}-scripts
    - user: root
    - mode: 755
    - template: jinja
    - require:
       - cmd: redis_compile

  cmd.run:
    - names:
      - chkconfig --add redis
      - chkconfig redis on
    - unless: chkconfig --list redis

#add_user:
#  user.present:
#    - name: redis
#    - gid_from_name: Ture
#    - createhome: Ture
#    - shell: /sbin/nologin
#    - unless: id redis

start_script:
  service.running:
    - name: redis
    - enable: True
    - reload: True
    - watch:
      - file: {{appserver}}/{{redis_ver}}/conf/redis.conf

