include:
  - set.init

{% set temp = pillar['temp'] %}
{% set appserver = pillar['appserver'] %}
{% set redis = pillar['redis'] %}
{% set redis_ver = pillar['redis_ver'] %}

redis_source:
  file.managed:
    - name: {{temp}}/{{redis}}
    - unless: test -e {{temp}}/{{redis}}
    - source: salt://redis/files/{{redis}}

extract_redis:
  cmd.run:
    - cwd: {{temp}}
    - name: tar zxf {{redis}}
    - unless: test -d {{temp}}/{{redis_ver}}
    - require:
      - file: redis_source

redis_compile:
  cmd.run:
    - cwd:  {{temp}}/{{redis_ver}}
    - name: make PREFIX={{appserver}}/{{redis_ver}} install
    - require:
      - cmd: extract_redis
    - unless: test -d {{appserver}}/{{redis_ver}}

