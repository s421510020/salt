haproxy-install:
  file.managed:
    - name: /usr/local/src/haproxy-1.4.24.tar.gz
    - source: salt://haproxy/files/haproxy-1.4.24.tar.gz
    - mode: 755
    - user: root
    - group: root
  cmd.run:
    - name: cd /usr/local/src && tar zxf haproxy-1.4.24.tar.gz && cd haproxy-1.4.24 && make TARGET=linux26 PREFIX=/jboss/haproxy-1.4.24 && make install PREFIX=/jboss/haproxy-1.4.24
    - unless: test -d /jboss/haproxy-1.4.24
    - require: 
      - file: haproxy-install

/etc/haproxy/haproxy.cfg:
  file.managed:
    - source: salt://haproxy/files/haproxy.conf
    - mode: 644
    - user: root
    - group: root

/etc/init.d/haproxy:
  file.managed:
    - source: salt://haproxy/files/haproxy.init
    - mode: 755
    - user: root
    - group: root
    - require:
      - cmd.run: haproxy-install

net.ipv4.ip_nonlocal_bind:
  sysctl.present: 
    - value: 1

haproxy-server:
  cmd.run:
    - name: chkconfig --add haproxy
    - unless: chkconfig --list | grep haproxy
    - require:
      - file: /etc/init.d/haproxy
  service.running:
    - name: haproxy
    - enable: True
    - watch: 
      - file: /etc/haproxy/haproxy.cfg
