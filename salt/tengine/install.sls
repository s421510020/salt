#nginx.tar.gz
nginx_source:
  file.managed:
    - name: /tmp/tengine-1.5.2.tar.gz
    - unless: test -e /tmp/tengine-1.5.2.tar.gz
    - source: salt://nginx/files/tengine-1.5.2.tar.gz

#pcre.tar.gz
pcre_source:
  file.managed:
    - name: /tmp/pcre-8.33.tar.gz
    - unless: test -e /tmp/pcre-8.33.tar.gz
    - source: salt://nginx/files/pcre-8.33.tar.gz

#extract
extract_nginx:
  cmd.run:
    - cwd: /tmp
    - names:
      - tar zxvf tengine-1.5.2.tar.gz
    - unless: test -d /tmp/tengine-1.5.2.tar.gz
    - require:
      - file: nginx_source

extract_pcre:
  cmd.run:
    - cwd: /tmp
    - names:
      - tar -zxvf pcre-8.33.tar.gz
    - unless: test -d /tmp/pcre-8.33.tar.gz
    - require:
      - file: pcre_source

#user
nginx_user:
  user.present:
    - name: nginx
    - uid: 1501
    - createhome: False
    - gid_from_name: True
    - shell: /sbin/nologin

#nginx_pkgs
nginx_pkg:
  pkg.installed:
    - pkgs:
      - gcc
      - openssl-devel
      - gcc-c++
      - zlib-devel


#pcre_compile
pcre_compile:
  cmd.run:
    - cwd: /tmp/pcre-8.33
    - names:
      - ./configure --prefix=/usr/local/pcre  
      - make
      - make install
    - require:
      - cmd: extract_pcre
    - unless: test -d /usr/local/pcre

#nginx_compile
nginx_compile:
  cmd.run:
    - cwd: /tmp/tengine-1.5.2
    - names:
      - ./configure --prefix=/usr/local/nginx  --user=nginx  --group=nginx   --with-http_stub_status_module --with-http_ssl_module --with-http_gzip_static_module --with-ipv6  --with-pcre=/tmp/pcre-8.33
      - make
      - make install
    - require:
      - cmd: extract_nginx
      - pkg: nginx_pkg
    - unless: test -d /usr/local/nginx

#cache_dir
cache_dir:
  cmd.run:
    - names:
      - chown -R nginx.nginx /usr/local/nginx/ && mkdir -p /usr/local/nginx/conf/vhosts
    - require:
      - cmd: nginx_compile
