/jboss/tengine-1.4.2/conf/nginx.conf:
  file.managed:
    {% if grains['fqdn'] in '[bj-web-node1.womai.com,bj-web-node2.womai.com,bj-web-node3.womai.com,bj-web-node4.womai.com,bj-web-node5.womai.com]' %}
    - source: salt://nginx/files/bj-nginx.conf
    {% elif grains['fqdn'] in '[sh-web-node1.womai.com,sh-web-node2.womai.com,sh-web-node3.womai.com,sh-web-node4.womai.com]' %}
    - source: salt://nginx/files/sh-nginx.conf
    {% elif grains['fqdn'] in '[gz-web-node1.womai.com,gz-web-node2.womai.com,gz-web-node3.womai.com,gz-web-node4.womai.com]' %}
    - source: salt://nginx/files/gz-nginx.conf
    {% endif %}
    - user: jboss
    - group: jboss
    - mode: 644
    - template: jinja
    - defaults:
      IPADDR: {{ salt['network.ip_addrs']()[0] }}

/jboss/tengine-1.4.2/conf/acl.conf:
  file.managed:
    - source: salt://nginx/files/acl.conf
    - user: jboss
    - group: jboss
    - mode: 644

nginx-server:
  file.managed:
    - name: /etc/init.d/nginx
    - source: salt://nginx/files/nginx-init
    - mode: 755
    - user: root
    - group: root
  cmd.run:
    - name: chkconfig --add nginx
    - unless: chkconfig --list | grep nginx
    - require:
      - file: nginx-server
  service.running:
    - name: nginx
    - enable: True
    - reload: True
    - watch:
      - file: /jboss/tengine-1.4.2/conf/nginx.conf
      - file: /jboss/tengine-1.4.2/conf/acl.conf
    - require:
      - cmd.run: nginx-server
