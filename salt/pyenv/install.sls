git_install:
  pkg.installed:
    - pkgs: 
      - git


pyenv_install:
  cmd.run:
    - cwd: /root/
    - names: 
      - git clone git://github.com/yyuu/pyenv.git ~/.pyenv && git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv && echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc && echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc && echo 'eval "$(pyenv init -)"' >> ~/.bashrc  && exec $SHELL -l
    - unless: test -d ~/.pyenv
    - require:
      - pkg: git_install
