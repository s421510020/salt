pyenv-deps:
  pkg.installed:
    - pkgs:
      - make
      - wget
      - curl
      - llvm
      - git
  cmd.run:
    - name: git clone https://github.com/yyuu/pyenv-virtualenv.git /usr/local/pyenv/plugins/pyenv-virtualenv && echo 'export PYENV_ROOT="/usr/local/pyenv"' >> ~/.bashrc && echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc && echo 'eval "$(pyenv init -)"' >> ~/.bashrc  && exec $SHELL -l
    - unless: grep  'export PYENV_ROOT="/usr/local/pyenv"' ~/.bashrc

#卸载python2.6
#python-2.6:
#  pyenv.absent:
#    - require:
#      - pkg: pyenv-deps
python-3.4.2:
  pyenv.installed:
    - user: root
    - require:
      - pkg: pyenv-deps
python-2.7.6:
  pyenv.installed:
    - user: root
    - default: True
    - require:
      - pkg: pyenv-deps