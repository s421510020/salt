#!/usr/local/bin/python
import os
import sys

tcp = sys.argv[1].strip()
d = {}
cmd = os.popen("netstat -n | awk '/^tcp/ {++S[$NF]} END {for(a in S) print a, S[a]}'")
for line in cmd.readlines():
	k,v = line.split()
	d[k] = v
if d.has_key(tcp):
	print d[tcp]
else:
	print "0"
