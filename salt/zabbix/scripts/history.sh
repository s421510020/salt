#设置history格式
export HISTTIMEFORMAT="[%Y-%m-%d %H:%M:%S] [`who am i 2>/dev/null| awk '{print $NF}'|sed -e 's/[()]//g'`] "
#记录shell执行的每一条命令
export PROMPT_COMMAND='if [ -z "$OLD_PWD" ];then
    export OLD_PWD=$PWD;
fi;
if [ ! -z "$LAST_CMD" ] && [ "$(history 1)" != "$LAST_CMD" ]; then
    logger -t user:`whoami` "[$OLD_PWD]$(history 1)";
fi ;
export LAST_CMD="$(history 1)";
export OLD_PWD=$PWD;'

