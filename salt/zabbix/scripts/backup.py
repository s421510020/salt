import os
from subprocess import Popen, PIPE, STDOUT
import datetime
import logging
import smtplib
from email.mime.text import MIMEText
from email.header import Header
import threading

config = {
    'db_host': '192.168.11.189',
    'db_username': 'root',
    'db_passwd': 'qnsoft',
    'db_name': ['cms','xuanwen_user'],
    'db_tb': ['cms.tables','xuanwen_user.tables'],
    'date': datetime.date.today(),
    'to_addr': ['zhangzhao@channelst.com']

}

# logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s [%(threadName)s] %(message)s')


logger = logging.getLogger('dbbackup')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)

fh = logging.FileHandler('dbbackup.log')
fh.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s %(levelname)s [%(threadName)s] %(message)s')

fh.setFormatter(formatter)
ch.setFormatter(formatter)

logger.addHandler(ch)
logger.addHandler(fh)


def backup_db(db_name,tb):
    dir = os.path.abspath('{0}/{1}'.format(db_name, config['date']))
    if not os.path.isdir(dir):
        os.makedirs(dir)
    src_path = os.getcwd()
    if tb.split('.')[0] == db_name:
        with open(tb, 'r') as f:
            for line in f.readlines():
                table = line.rstrip('\n').strip(' ')
                os.chdir(dir)
                cmd = 'mysqldump -h{4} -u{0} -p{1} --single-transaction --opt {2} {3} > {3}.sql'.format(config['db_username'], config['db_passwd'],db_name, table,config['db_host'])
                data = Popen(cmd, stdout=PIPE, stderr=STDOUT, shell=True)
                content = data.stdout.read()
                os.chdir(src_path)
                if content:
                    logger.error(db_name + '--dbbackup: ' + line + 'Error InFo:  ' + str(content))
                    for tomail in config['to_addr']:
                        _Sendmail(tomail, str(content))
	     

    else:
        for tomail in config['to_addr']:
            _Sendmail(tomail, '数据库名字-{0}与表文件名前缀-{1}不匹配或着缺失!!!!'.format(db_name,tb))


def _Sendmail(to_addr, message):
    from_addr = 's421510020@163.com'
    # 授权码,非密码
    passwd = 'ss2286183'
    smtp_server = 'smtp.163.com'
    msg = MIMEText(message, 'plain', 'utf-8')
    msg['Subject'] = Header('189服务器数据库备份失败'.format(config['db_name']), 'utf-8').encode()
    msg['From'] = from_addr
    msg['To'] = to_addr
    server = smtplib.SMTP()
    server.connect(smtp_server)
    server.ehlo(smtp_server)
    # server.set_debuglevel(1)
    server.login(from_addr, passwd)
    server.sendmail(from_addr, to_addr, msg.as_string())
    server.quit()


if __name__ == '__main__':
        t1 = threading.Thread(target=backup_db(config['db_name'][0],config['db_tb'][0]), name='thread-1')
        t2 = threading.Thread(target=backup_db(config['db_name'][1],config['db_tb'][1]), name='thread-2')
        t1.setDaemon(True)
        t2.setDaemon(True)
        t2.start()
        t1.start()
        t1.join()
        t2.join()
