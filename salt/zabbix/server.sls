{% set zabbix = pillar['zabbix_server'] %}
{% set zabbix_ver = pillar['zabbix_ver'] %}
{% set temp = pillar['temp'] %}
{% set appserver = pillar['appserver'] %}

zabbix_agent:
  file.managed:
    - name: {{temp}}/{{zabbix}}
    - source: salt://zabbix/files/{{ zabbix }}
    - unless: test -e {{ temp }}/{{ zabbix }}

extract_zabbix:
  cmd.run:
    - cwd: {{ temp }}
    - names:
      - tar zxf {{ zabbix }}
    - unless: test -d {{ temp }}/{{ zabbix_ver }}
    - require:
      - file: zabbix_agent
#user
zabbix_user:
  user.present:
    - name: zabbix
    - uid: 15011
    - createhome: False
    - gid_from_name: True
    - shell: /sbin/nologin

#zabbix_pkgs
zabbix_pkg:
  pkg.installed:
    - pkgs:
      


#zabbix_compile
zabbix_compile:
  cmd.run:
    - cwd: {{ temp }}/{{ zabbix_ver }}
    - names:
      - ./configure --prefix={{appserver}}/{{zabbix_ver}} --enable-server --enable-agent --with-mysql --enable-ipv6 --with-net-snmp --with-libcurl --with-libxml2 -with-openipmi

 && make && make install 
    - require:
      - cmd: extract_zabbix
      #- pkg: zabbix_pkg
    - unless: test -d {{ appserver }}/{{ zabbix_ver }}

