include:
  - zabbix.agent

{% set zabbix = pillar['zabbix_server'] %}
{% set zabbix_ver = pillar['zabbix_ver'] %}
{% set temp = pillar['temp'] %}
{% set appserver = pillar['appserver'] %}

zabbix-config:
  file.managed:
    - name: {{appserver}}/{{zabbix_ver}}/etc/zabbix_agentd.conf
    - source: salt://zabbix/files/zabbix_agentd.conf
    - template: jinja
    - defaults:
      server: 10.0.0.10
    - require:
      - cmd: extract_zabbix

zabbix-init:
  file.managed:
    - name: /etc/init.d/zabbix_agentd
    - mode: 755
    - user: root
    - template: jinja
    - source: salt://zabbix/files/zabbix-init
  service.running:
    - name: zabbix_agentd
    - enable: True
    - reload: True
    - watch:
      - file: {{appserver}}/{{zabbix_ver}}/etc/zabbix_agentd.conf
    - require:
      - file: zabbix-init




