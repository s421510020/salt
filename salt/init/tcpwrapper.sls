/etc/hosts.deny:
  file:
    - managed
    - source: salt://init/files/hosts.deny
    - mode: 644
    - user: root
    - group: root
/etc/hosts.allow:
  file:
    - managed
    - source: salt://init/files/hosts.allow
    - mode: 644
    - user: root
    - group: root
