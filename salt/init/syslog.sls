syslog:
  file.managed:
    - name: /etc/{{ pillar['syslog']['syslog'] }}.conf
    - source: salt://init/files/{{ pillar['syslog']['syslog'] }}.conf
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    - defaults:
      LOG_SERVER1: {{ pillar['syslog']['LOG_SERVER1'] }}
      LOG_SERVER2: {{ pillar['syslog']['LOG_SERVER2'] }}
  service.running:
    - name: {{ pillar['syslog']['syslog'] }}
    - enable: True
    - watch:
      - file: syslog

/etc/bashrc:
  file.append:
    - text:
      - export PROMPT_COMMAND='{ msg=$(history 1 | { read x y; echo $y; });logger "[euid=$(whoami)]":$(who am i):[`pwd`]"$msg"; }'
