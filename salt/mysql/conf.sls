include:
  - mysql.install

{% set mysql_ver = pillar['mysql_ver'] %}
{% set appserver = pillar['appserver'] %}

mysql_conf:
  file.managed:
    - name: {{appserver}}/{{mysql_ver}}/my.cnf
    - user: root
    - mode: 755
    - source: salt://mysql/files/my.cnf
    - template: jinja


##service for mysql

tran_script:
  cmd.run:
    - cwd: {{appserver}}/{{mysql_ver}}/support-files/
    - names: 
      - cp -f mysql.server /etc/init.d/mysqld
      - chmod +x /etc/init.d/mysqld
    - unless: test -x /etc/init.d/mysqld
    - require:
      - pkg: mysql_pkg
start_script:
  cmd.run:
    - cwd: {{appserver}}/{{mysql_ver}}
    - names:
      - ./scripts/mysql_install_db --user=mysql --basedir={{appserver}}/{{mysql_ver}} --datadir={{appserver}}/{{mysql_ver}}/data
  service.running:
    - name: mysqld
    - enable: True
    - reload: True
    - watch:
      - file: {{appserver}}/{{mysql_ver}}/my.cnf
ln_sock:
  cmd.run:
    - names:
      - mkdir -p /var/lib/mysql
      - ln -sv {{appserver}}/{{mysql_ver}}/data/mysql/mysqld.sock /var/lib/mysql/mysql.sock
    - unless: test -S /var/lib/mysql/mysql.sock
    - require:
      - cmd: start_script 
