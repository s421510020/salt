include:
  - set.init

mysql_source:
  file.managed:
    - name: {{ pillar['temp'] }}/mysql-5.5.36.tar.gz
    - unless: test -e {{ pillar['temp'] }}/mysql-5.5.36.tar.gz
    - source: salt://mysql/files/mysql-5.5.36.tar.gz
#extract_mysql:
#  cmd.run:
#    - cwd: /home/xuanwen/temp
#    - names: 
#      - tar xf mysql-5.5.36.tar.gz
#      - chown: root.root /home/xuanwen/appserver/mysql* -R
#    - unless: test -d mysql-5.5.36
#    - require:
#      - file: mysql_source
#mysql_user:
#  user.present:
#    - name: mysql
#    - uid: 1024
#    - createhome: True
#    - gid_from_home: True
#    - shell: /sbin/nologin
#mysql_pkg:
#  pkg.installed:
#    - pkgs:
#      - gcc
#      - gcc-c++
#      - autoconf
#      - automake
#      - openssl
#      - openssl-devel
#      - cmake
#      - zlib
#      - zlib-devel
#      - ncurses-devel
#      - libtool-ltdl-devel
#mysql_commplie:
#  cmd.run:
#    - cwd: /home/xuanwen/temp/mysql-5.5.36
#    - names:
#      - cmake . -DCMAKE_INSTALL_PREFIX=/home/xuanwen/appserver/mysql -DMYSQL_DATADIR=/home/xuanwen/appserver/mysql/xuanwen/data -DSYSCONFDIR=/etc  -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_ARCHIVE_STORAGE_ENGINE=1 -DWITH_BLACKHOLE_STORAGE_ENGINE=1 -DWITH_READLINE=1 -DWITH_SSL=system -DWITH_ZLIB=system -DWITH_LIBWRAP=0 -DMYSQL_UNIX_ADDR=/tmp/mysql.sock -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci
#      - make
#      - make install
#    - require:
#      - cmd: extract_mysql
#      - pkg: mysql_pkg
#    - unless: test -d /home/xuanwen/appserver/mysql
