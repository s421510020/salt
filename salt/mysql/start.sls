start_server:
  file.managed:
    - name: /etc/init.d/mysqld
    - source: salt://mysql/files/mysqld
  cmd.run:
    - names: 
       - chkconfig --add mysqld
       - chkconfig mysqldd on
    - unless: chkconfig --list mysqld
  service.running:
    - name: mysqld
    - enable: True
