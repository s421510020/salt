include:
  - set.init

{% set temp = pillar['temp'] %}
{% set appserver = pillar['appserver'] %}
{% set mysql = pillar['mysql'] %}
{% set mysql_ver = pillar['mysql_ver'] %}
{% set user = pillar['user'] %}

mysql_source:
  file.managed:
    - name: {{ temp }}/{{ mysql }}
    - unless: test -e {{ temp }}/{{ mysql }}
    - source: salt://mysql/files/{{ mysql }} 
extract_mysql:
  cmd.run:
    - cwd: {{ temp }}
    - names: 
      - tar xf {{ mysql }}
#not found result for problem
#      - chown: {{user}}.root {{ appserver }}/mysql* -R
    - unless: test -d {{ mysql_ver }}
    - require:
      - file: mysql_source
mysql_user:
  user.present:
    - name: mysql
    - uid: 27
    - createhome: False
    - gid_from_name: True
    - shell: /sbin/nologin
mysql_pkg:
  pkg.installed:
    - pkgs:
      - gcc
      - gcc-c++
      - autoconf
      - automake
      - openssl
      - openssl-devel
      - cmake
      - zlib
      - zlib-devel
      - ncurses-devel
      - libtool-ltdl-devel
mysql_commplie:
  cmd.run:
    - cwd: {{ temp }}/{{ mysql_ver }}
    - names:
      - cmake -DCMAKE_INSTALL_PREFIX={{ appserver }}/{{ mysql_ver }} -DMYSQL_DATADIR={{ appserver }}/{{ mysql_ver }}/data -DSYSCONFDIR=/etc  -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_ARCHIVE_STORAGE_ENGINE=1 -DWITH_BLACKHOLE_STORAGE_ENGINE=1 -DWITH_READLINE=1 -DWITH_SSL=system -DWITH_ZLIB=system -DWITH_LIBWRAP=0 -DMYSQL_UNIX_ADDR=/tmp/mysql.sock -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci && make && make install
    - require:
      - cmd: extract_mysql
      - pkg: mysql_pkg
    - unless: test -d {{ appserver }}/{{ mysql_ver }}
