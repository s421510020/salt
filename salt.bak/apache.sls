
apache:
  pkg:
    - installed
  server:
    - running
    - require:
    - pkg: httpd
  file.managed:
    - source: salt://apache/httpd.conf
    - name: /etc/httpd/conf/httpd.conf
    - user: root
    - group: root
    - mode: 644
