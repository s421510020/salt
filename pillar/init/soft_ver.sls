#variables for mysql
#
mysql: mysql-5.5.36.tar.gz
mysql_ver: mysql-5.5.36

#variables for nginx
nginx: nginx-1.10.1.tar.gz
nginx_ver: nginx-1.10.1

pcre: pcre-8.39.tar.gz
pcre_ver: pcre-8.39

#variables for php
php: php-5.5.38.tar.gz
php_ver: php-5.5.38

# variables for redis
#
redis: redis-3.0.7.tar.gz
redis_ver: redis-3.0.7

# variables for memcached

memcached: memcached-1.4.30.tar.gz
memcached_ver: memcached-1.4.30
